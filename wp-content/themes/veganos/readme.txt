Theme Name: Veganos
Theme URI: https://www.anarieldesign.com/free-food-blogger-wordpress-theme/
Author: Anariel Design
Author URI: https://www.anarieldesign.com/
Description: Veganos is a wonderfully designed, clean and responsive free WordPress theme built using the new Gutenberg editor. Veganos is perfect for creating food-related websites like recipe websites, blogs, magazines and more. It is built mobile first and is pleasure to view on devices of all sizes. It features modern, easy-to-read typography and minimalistic design.
Version: 1.0.6
Tags: accessibility-ready, custom-colors, custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments 
Requires at least: 4.9
Tested up to:  5.0

== Copyright License ==
Veganos 100% GPL and/or 100% GPL-compatible licensed.
Declare copyright and license explicitly. Use the license and license uri header slugs to style.css.
Declare licenses of any resources included such as fonts or images.
All code and design should be your own or legally yours. Cloning of designs is not acceptable.
Any copyright statements on the front end should display the user's copyright, not the theme author's copyright.

Veganos WordPress Theme, Copyright 2018 Anariel Design
Veganos is distributed under the terms of the GNU GPL

This theme is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

== Description ==
Veganos is a wonderfully designed, clean and responsive free WordPress theme built using the new WordPress editor. Veganos is perfect for creating food-related websites like recipe websites, blogs, magazines and more. It is built mobile first and is pleasure to view on devices of all sizes. It features modern, easy-to-read typography and minimalistic design.

== Credits ==

All the theme files, scripts and images are licensed under GPLv2 license
Veganos theme uses:
* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* Genericons icon font, Copyright 2013 Automattic
Genericons are licensed under the terms of the GNU GPL, Version 2 (or later)
Source: http://www.genericons.com
* Google Fonts
Poppins: Open Font License 
Designer: Indian Type Foundry
Source: https://www.google.com/fonts

=== Image Credits ==

* Screenshot Image, demo images and illustration: Anariel Design
* Testimonials page image: CC0 by raw pixel  via Pixabay (https://pixabay.com/en/african-bangkok-business-3402716/)

== Theme Features ==

* Responsive design
* WordPress Theme Customizer support
* SEO friendly
* Header Image
* Ready for the new WordPress editor
* Cross-browser compatibility
* Mobile Menu

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme includes documentation? =
Yes it does, you can find it here: https://www.anarieldesign.com/documentation/veganos/

== Changelog ==

Version 1.0.6 - December 9, 2018
- fix for the translation function in the site-info.php file
- few fixes for the escaping

Version 1.0.5 - November 2, 2018
- update for Block Gallery styles
- improved image/gallery block styles

Version 1.0.3, 1.0.4 - October 30, 2018
- general Gutenberg style touchups and support for Block Gallery
- new description text

Version 1.0.1, 1.0.2 - October 12, 2018
- accessibility fixes
- fixes for escaping

Version 1.0.0 - October 08, 2018












